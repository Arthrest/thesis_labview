
enum{
  AQW_PIN1 = 2,
  AQW_PIN2 = 3,
  DG_PIN1 = 4,
  DG_PIN2 = 5,
  DG_PIN3 = 6,
  DG_PIN4 = 7,
  ADG_PIN1 = 8,
  ADG_PIN2 = 9,
  ADG_PIN3 = 10,
  ADG_PIN4 = 11,
  RELAY_PIN1 = 12,
  RELAY_PIN2 = 13  
};


int frequency;

bool AQW_STATUS_PIN1;
bool AQW_STATUS_PIN2;
bool DG_STATUS_PIN1;
bool DG_STATUS_PIN2;
bool DG_STATUS_PIN3;
bool DG_STATUS_PIN4;
bool ADG_STATUS_PIN1;
bool ADG_STATUS_PIN2;
bool ADG_STATUS_PIN3;
bool ADG_STATUS_PIN4;
bool RELAY_STATUS_PIN1;
bool RELAY_STATUS_PIN2;

void SCPI_ProcTask(void);
static void activeDevice(int deviceNo, bool deviceStatus);
static void deviceSetPeirod(int device, int peirod);
static bool SCPI_readLine(char* data);

//////////////////////////Inits/////////////////////////////////////////////
void setup() {
Serial.begin(115200);
Serial.println("Device ready");
pinMode(AQW_PIN1, OUTPUT);
pinMode(AQW_PIN2, OUTPUT);
pinMode(DG_PIN1, OUTPUT);
pinMode(DG_PIN2, OUTPUT);
pinMode(DG_PIN3, OUTPUT);
pinMode(DG_PIN4, OUTPUT);
pinMode(ADG_PIN1, OUTPUT);
pinMode(ADG_PIN2, OUTPUT);
pinMode(ADG_PIN3, OUTPUT);
pinMode(ADG_PIN4, OUTPUT);
pinMode(RELAY_PIN1, OUTPUT);
pinMode(RELAY_PIN2, OUTPUT);
}

//////////////////////////Main loop/////////////////////////////////////////////
void loop() {
  SCPI_ProcTask();

  if(Serial.available() == 0){
    if(AQW_STATUS_PIN1 == true){
      digitalWrite(AQW_PIN1, HIGH);
    }
    if(AQW_STATUS_PIN2 == true){
      digitalWrite(AQW_PIN2, HIGH);
    }
    if(DG_STATUS_PIN1 == true){
      digitalWrite(DG_PIN1, HIGH);
    }
    if(DG_STATUS_PIN2 == true){
      digitalWrite(DG_PIN2, HIGH);
    }
    if(DG_STATUS_PIN3 == true){
      digitalWrite(DG_PIN3, HIGH);
    }
    if(DG_STATUS_PIN4 == true){
      digitalWrite(DG_PIN4, HIGH);
    }
    if(ADG_STATUS_PIN1 == true){
      digitalWrite(ADG_PIN1, HIGH);
    }
    if(ADG_STATUS_PIN2 == true){
      digitalWrite(ADG_PIN2, HIGH);
    }
    if(ADG_STATUS_PIN3 == true){
      digitalWrite(ADG_PIN3, HIGH);
    }
    if(ADG_STATUS_PIN4 == true){
      digitalWrite(ADG_PIN4, HIGH);
    }
    if(RELAY_STATUS_PIN1 == true){
      digitalWrite(RELAY_PIN1, HIGH);
    }
    if(RELAY_STATUS_PIN1 == true){
      digitalWrite(RELAY_PIN2, HIGH);
    }
    delay(frequency/2);
  
    if(AQW_STATUS_PIN1 == true){
      digitalWrite(AQW_PIN1, LOW);
    }
    if(AQW_STATUS_PIN2 == true){
      digitalWrite(AQW_PIN2, LOW);
    }
    if(DG_STATUS_PIN1 == true){
      digitalWrite(DG_PIN1, LOW);
    }
    if(DG_STATUS_PIN2 == true){
      digitalWrite(DG_PIN2, LOW);
    }
    if(DG_STATUS_PIN3 == true){
      digitalWrite(DG_PIN3, LOW);
    }
    if(DG_STATUS_PIN4 == true){
      digitalWrite(DG_PIN4, LOW);
    }
    if(ADG_STATUS_PIN1 == true){
      digitalWrite(ADG_PIN1, LOW);
    }
    if(ADG_STATUS_PIN2 == true){
      digitalWrite(ADG_PIN2, LOW);
    }
    if(ADG_STATUS_PIN3 == true){
      digitalWrite(ADG_PIN3, LOW);
    }
    if(ADG_STATUS_PIN4 == true){
      digitalWrite(ADG_PIN4, LOW);
    }
    if(RELAY_STATUS_PIN1 == true){
      digitalWrite(RELAY_PIN1, LOW);
    }
    if(RELAY_STATUS_PIN1 == true){
      digitalWrite(RELAY_PIN2, LOW);
    }
    delay(frequency/2);
  }
}

//////////////////////////SCPI/////////////////////////////////////////////
enum {
  SCPI_COMMAND_BUFFER_LENGTH = 100
};

static char commandBuffer[SCPI_COMMAND_BUFFER_LENGTH];

void SCPI_ProcTask(void) {
    if(true == SCPI_readLine(commandBuffer)) {
      int value;
      int device;
      char* delimPos = strchr(commandBuffer, ':');
      if(delimPos != 0) {
      *delimPos = '\0';
        if(0 == strcmp(commandBuffer, "ACTIVE")) {
          device = atoi(delimPos + 1);
          memset(commandBuffer,0,sizeof(commandBuffer));
          activeDevice(device, true);
          Serial.print("active device: ");
          Serial.println(device, DEC);
        }
        else if(0 == strcmp(commandBuffer, "DEACTIVE")) {
          device = atoi(delimPos + 1);
          memset(commandBuffer,0,sizeof(commandBuffer));
          activeDevice(device, false);
          Serial.print("deactive device: ");
          Serial.println(device, DEC);
        }
        else if(0 == strcmp(commandBuffer, "SET")) {
          value = atoi(delimPos + 1);
          memset(commandBuffer,0,sizeof(commandBuffer));
          frequency = value;
          Serial.print("set period to: ");
          Serial.println(value, DEC);
        }
      else if(0 == strcmp(commandBuffer, "*IDN?")){
        Serial.print("CONTROLLER\n");
        memset(commandBuffer,0,sizeof(commandBuffer));
      }
    }
  }
}
//////////////////////////Static functions/////////////////////////////////////////////

static bool SCPI_readLine(char* data){
  static size_t i;
  char c;
  
  if(i < SCPI_COMMAND_BUFFER_LENGTH) {
    if(Serial.available() > 0){
      c = Serial.read();
        switch(c) {
        case '\r': // Fallthrough
        case '\n':
          commandBuffer[i] = '\0';
          i = 0;
          return true;
          break;
        default:
          commandBuffer[i++] = c;
        }
    }
  return false;
  }
}

static void activeDevice(int deviceNo, bool deviceStatus){
  switch(deviceNo){
    case AQW_PIN1:
    AQW_STATUS_PIN1 = deviceStatus;
    break;
    case AQW_PIN2:
    AQW_STATUS_PIN2 = deviceStatus;
    break;
    case DG_PIN1:
    DG_STATUS_PIN1 = deviceStatus;
    break;
    case DG_PIN2:
    DG_STATUS_PIN2 = deviceStatus;
    break;
    case DG_PIN3:
    DG_STATUS_PIN3 = deviceStatus;
    break;
    case DG_PIN4:
    DG_STATUS_PIN4 = deviceStatus;
    break;
    case ADG_PIN1:
    ADG_STATUS_PIN1 = deviceStatus;
    break;
    case ADG_PIN2:
    ADG_STATUS_PIN2 = deviceStatus;
    break;
    case ADG_PIN3:
    ADG_STATUS_PIN3 = deviceStatus;
    break;
    case ADG_PIN4:
    ADG_STATUS_PIN4 = deviceStatus;
    break;
    case RELAY_PIN1:
    RELAY_STATUS_PIN1 = deviceStatus;
    break;
    case RELAY_PIN2:
    RELAY_STATUS_PIN1 = deviceStatus;
    break;
  }
}
